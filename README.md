### About
This is the code for the website [https://lissarchive.org](https://lissarchive.org), an informational site for the LIS Scholarship Archive, a repository for LIS work. This site is built using [Nikola](https://getnikola.com/), a static site generator that relies on python3. The instructions for compiling and building the website are below.

### Building
I would recommend you use a virtualenv to build and view this website. This is a Python tool to create isolated Python environments. The HitchHiker's Guide to Python has a [great guide](http://docs.python-guide.org/en/latest/dev/virtualenvs/) on virtual environments that I used to learn how to use/interact with virtualenvs. 

Here's how to make and activate a virtual environment:
<pre><code># install the tool virtualenv
$ pip install virtualenv

# create the Python 3 virtual environment
$ virtualenv -p python3 venv

# activate the virtual environment
$ source venv/bin/activate
</pre></code>

Now, you can get started and install all of the dependecies of this site in its own environment:

<pre><code># install the dependencies
$ pip install Nikola['extras']

# clone this repo
$ git clone git@gitlab.com:VickySteeves/lissarchive.git

# change directory (cd) so you are in the right folder for the website
$ cd lissarchive

# build the website
$ nikola build

# see the website
$ nikola serve -b
</pre></code>